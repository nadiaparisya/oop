class Animal {
 
    constructor(name, hungry) {
               this.name = name;
               this.hungry = hungry;
           }
          
          doEat() {
              return console.log("Eat");
          }
          
          doMove() {
              return console.log("Move");
          }
        }

       class Dog extends Animal {
           constructor(name, hungry) {
              super(name);
              super(hungry); 
           }
   
           doHowl(){
               return console.log("Howl");
           }
       }

       class Bird extends Animal {
           constructor(name, hungry) {
              super(name);
              super(hungry); 
           }
           doFly(){
               return console.log("Fly");
           }
       }

           const cihuahua = new Dog("cihuahua", true);
           cihuahua.doEat();
           cihuahua.doMove();
           cihuahua.doHowl();

           const kakaktua = new Bird("kakaktua", false);
           kakaktua.doEat();
           kakaktua.doMove();
           kakaktua.doFly();